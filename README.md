# Demo API

## Запуск

Docker окружение рассчитано на development.
Для старта необходимо скопировать .env.dist в .env

```cp ./.env.dist ./.env```

Запуск контейнеров

```docker-compose up -d```

Установка зависимостей

```docker-compose run api composer install```

Выполнение миграций

```docker-compose run api ./vendor/bin/doctrine-migrations migrations:migrate```

## API

Все методы выполняются методом POST

### Order


```/order/create```
###### Параметры
- products[]

Создает заказ. Необходимо передать массив 'products' с идентификаторами по которым необходимо создать заказ

```/order/pay/{id}```
###### Параметры
 - (int) sum
 
Оплатить заказ необходимо передать сумму для оплаты параметром sum  

### Product

```/product/create```

Создает 20 товаров в базе данных 
