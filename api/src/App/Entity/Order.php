<?php


namespace App\Entity;

/**
 * Class Order
 * @Entity
 * @Table(name="demo_order")
 * @package App\Entity
 */
class Order
{
    /** @var int Id Статуса по умолчанию */
    const NEW = 1;

    /** @var int Id Статуса по умолчанию */
    const PAID = 2;

    /**
     * @id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    public $id;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $status;

    /**
     * @ManyToMany(targetEntity="App\Entity\Product",inversedBy="orders")
     * @JoinTable(name="demo_order_products")
     * @var
     */
    private $products;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @Column(type="integer",nullable=true)
     * @var int
     */
    public $total;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function addProduct(Product $product)
    {
        $this->products->add($product);
        return $this;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
        return $this;
    }

    /**
     * @param array $products
     * @return $this
     */
    public function addProducts(array $products)
    {
        foreach ($products as $product) {
            $this->products->add($product);
        }

        return $this;
    }

    /**
     * @param array $products
     * @return $this
     */
    public function removeProducts(array $products)
    {
        foreach ($products as $product) {
            $this->products->removeElement($product);
        }
        return $this;
    }

}
