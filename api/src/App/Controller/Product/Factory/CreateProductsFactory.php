<?php


namespace App\Controller\Product\Factory;


use App\Controller\Product\CreateProductsController;
use App\Service\ProductService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Class CreateProductsFactory
 * @package App\Controller\Product\Factory
 */
class CreateProductsFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CreateProductsController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new CreateProductsController($container->get(ProductService::class));
    }
}
