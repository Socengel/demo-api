<?php


namespace App\Controller\Product;


use App\Service\ProductService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateProductsController
{

    /**
     * @var ProductService $productService
     */
    protected $productService;

    /**
     * CreateProductsController constructor.
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @throws \Exception
     */
    public function __invoke(Request $request, Response $response)
    {
        $names = [
            'продукт',
            'товар',
            'Инструмент'
        ];

        $this->productService->createProducts($names);
    }

}
