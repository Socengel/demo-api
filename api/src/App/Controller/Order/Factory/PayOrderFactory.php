<?php


namespace App\Controller\Order\Factory;


use App\Controller\Order\PayOrderController;
use App\Service\OrderService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Class PayOrderFactory
 * @package App\Controller\Order\Factory
 */
class PayOrderFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PayOrderController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new PayOrderController(
            $container->get(OrderService::class)
        );
    }

}
