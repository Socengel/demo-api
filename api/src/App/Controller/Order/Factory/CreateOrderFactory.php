<?php

namespace App\Controller\Order\Factory;


use App\Controller\Order\CreateOrderController;
use App\Service\OrderService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Class CreateOrderFactory
 * @package App\Controller\Order\Factory
 */
class CreateOrderFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CreateOrderController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new CreateOrderController(
            $container->get(OrderService::class)
        );
    }

}
