<?php


namespace App\Controller\Order;


use App\Service\OrderService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PayOrderController
{
    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * CreateOrderController constructor.
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $params
     * @throws \Exception
     */
    public function __invoke(Request $request, Response $response, $params)
    {
        $this->orderService->payOrder($params['id'], $request->get('summ'));
    }

}
