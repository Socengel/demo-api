<?php


namespace App\Controller\Order;


use App\Service\OrderService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CreateOrderController
 * @package App\Controller\Order
 */
class CreateOrderController
{
    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * CreateOrderController constructor.
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return int
     * @throws \Exception
     */
    public function __invoke(Request $request, Response $response): int
    {
        return $this->orderService->createOrder($request->request->get('products') ?: []);
    }

}
