<?php


namespace App\Service;


use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;

/**
 * Class OrderService
 * @package App\Service
 */
class OrderService
{
    /**
     * @var EntityManager $entityManager
     */
    protected $entityManager;

    /**
     * OrderService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->setEntityManager($entityManager);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Создает заказ с указанными идентификаторами товаров
     *
     * @param array $ids
     * @return int
     * @throws \Exception
     */
    public function createOrder(array $ids)
    {
        $productsCount = count($ids);
        if (!$productsCount) {
            throw new \Exception('Please specify product ids for order');
        }

        $products = $this->getEntityManager()->getRepository(Product::class)->findBy(['id' => $ids]);

        if ($productsCount != count($products)) {
            $foundIds = [];

            foreach ($products as $product) {
                $foundIds[] = $product->getId();
            }

            $missedIds = array_diff($ids, $foundIds);
            throw new \Exception('Product id not found:' . implode(',', $missedIds), 400);
        }

        try {
            $this->getEntityManager()->beginTransaction();
            $order = new Order();

            // Устанавливаем статус
            $order->setStatus(Order::NEW);

            // Пересчитываем по текущим ценам, подразумевается неизменность цен или резервация цены во время заказа
            // При изменении цен в будущем можно находить неоплаченные заказы с изменяемыми товарами и пересчитывать корзину.
            $total = 0;
            /**
             * @var Product $product
             */
            foreach ($products as $product) {
                $order->addProduct($product);
                $total += $product->getPrice();
            }
            $order->setTotal($total);

            $this->getEntityManager()->persist($order);
            $this->getEntityManager()->flush();
            $this->getEntityManager()->commit();

            return $order->getId();
        } catch (\Exception $e) {
            $this->getEntityManager()->rollback();
        }

    }

    /**
     * Оплачивает заказ с указанным id если сумма совпадает
     *
     * @param $id
     * @param $sum
     * @return bool
     * @throws \Exception
     */
    public function payOrder($id, $sum)
    {
        /** @var Order $order */
        $order = $this->getEntityManager()->getRepository(Order::class)->find($id);

        if (!$order) {
            throw new \Exception('Order does not exists', 404);
        }

        if ($order->getStatus() == Order::PAID) {
            throw new \Exception('Order already payed', 400);
        }

        if ($order->getTotal() == $sum) {
            $httpClient = new Client();
            try {
                $response = $httpClient->get('https://ya.ru');
            } catch (\Exception $e) {
                throw new \Exception('Payment gate error');
            }

            if ($response->getStatusCode() == 200) {
                try {
                    $this->getEntityManager()->beginTransaction();
                    $order->setStatus(Order::PAID);
                    $this->getEntityManager()->persist($order);
                    $this->getEntityManager()->flush();
                    $this->getEntityManager()->commit();
                    return true;
                } catch (\Exception $e) {
                    $this->getEntityManager()->rollback();
                    throw new \Exception('Database error', 500);
                }
            }
        } else {
            throw new \Exception('Payment sum not correct (current sum :' . $order->getTotal() . ')');
        }

        throw new \Exception('Payment error', '400');
    }

}
