<?php


namespace App\Service;


use App\Entity\Product;
use Doctrine\ORM\EntityManager;

/**
 * Class ProductSercvice
 * @package App\Service
 */
class ProductService
{
    /**
     * @var EntityManager $entityManager
     */
    protected $entityManager;

    /**
     * OrderService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->setEntityManager($entityManager);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Создает тоывары со случайными названиями
     *
     * @param array $names
     * @param int $count
     * @throws \Exception
     */
    public function createProducts(array $names, $count = 20)
    {
        try {
            for ($i = 0; $i < $count; $i++) {
                $product = new Product();
                $product->setName($names[rand(0, count($names) - 1)] . ' ' . rand(0, 1000));
                $product->setPrice(rand(0, 100000));
                $this->getEntityManager()->persist($product);
            }

            $this->getEntityManager()->flush();
        } catch (\Exception $e) {
            throw new \Exception('Database error', 500);
        }
    }
}
