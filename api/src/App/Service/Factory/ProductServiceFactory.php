<?php

namespace App\Service\Factory;


use App\Service\ProductService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Class ProductServiceFactory
 * @package App\Service\Factory
 */
class ProductServiceFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|void
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ProductService($container->get(EntityManager::class));
    }


}
