<?php

namespace App\Service\Factory;


use App\Service\OrderService;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Class OrderServiceFactory
 * @package App\Service\Factory
 */
class OrderServiceFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|void
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new OrderService($container->get(EntityManager::class));
    }

}
