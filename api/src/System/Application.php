<?php

namespace System;

use Laminas\ServiceManager\ServiceManager;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Matcher\UrlMatcher;

/**
 * Class Application
 */
class Application
{
    /**
     * Сервис менеджер
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * Логгер
     * @var LoggerInterface $logger
     */
    protected $logger;

    /**
     * Инициализация приложения
     * @param array $config Конфигурация приложения
     * @return Application
     */
    public static function init($config): self
    {
        $serviceManager = new ServiceManager($config['service_manager']);
        $serviceManager->setService('Config', $config);
        return new Application($serviceManager);
    }

    /**
     * Application constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    /**
     * Возвращает текущий контейнер приложения
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * Устанавливает контейнер приложения
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }

    /**
     * Возвращает текущий логгер
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        if (!$this->logger) {
            $this->setLogger($this->getContainer()->get(LoggerInterface::class));
        }

        return $this->logger;
    }

    /**
     * Устанавливает логгер приложения
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * Обрабатывает текущий Http Запрос
     */
    public function run(): void
    {
        /** @var UrlMatcher $urlMatcher */
        $urlMatcher = $this->container->get(UrlMatcher::class);

        $request = $this->getContainer()->get(Request::class);
        $response = new Response();

        try {
            $match = $urlMatcher->matchRequest($request);
            $controller = $this->container->get($match['_controller']);
            $result = $controller($request, $response, $match);
            $response->setContent(json_encode(['status' => 'ok', 'result' => $result != null ? $result : true]));
        } catch (\Exception $e) {
            $this->getLogger()->error('SYSTEM_ERROR', [
                'exception' => $e
            ]);

            switch (get_class($e)) {
                case MethodNotAllowedException::class:
                    $response->setStatusCode(405);
                    $response->setContent(json_encode(['status' => 'error', 'message' => 'Method not allowed']));
                    break;
                default:
                    $response->setStatusCode($e->getCode() ?: 500);
                    $response->setContent(json_encode(['status' => 'error', 'message' => $e->getMessage()]));
                    break;
            }
        }

        $response->send();
    }

}

