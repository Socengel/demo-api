<?php


namespace System\Factory;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class UrlMatcherFactory
 * @package System\Factory
 */
class UrlMatcherFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|UrlMatcher
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new UrlMatcher(
            $container->get(RouteCollection::class),
            (new RequestContext())->fromRequest($container->get(Request::class))
        );
    }

}
