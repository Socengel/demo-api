<?php


namespace System\Factory;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequestFactory
 * @package System\Factory
 */
class RequestFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|Request
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return Request::createFromGlobals();
    }

}
