<?php


namespace System\Factory;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Monolog\Handler\AmqpHandler;
use Monolog\Logger;
use PhpAmqpLib\Channel\AMQPChannel;

/**
 * Class LogFactory
 * @package System\Factory
 */
class LogFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Logger|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return (new Logger('common'))->pushHandler(new AmqpHandler($container->get(AMQPChannel::class), 'logs'));
    }

}
