<?php


namespace System\Factory;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteCollectionFactory
 * @package System\Factory
 */
class RouteCollectionFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|RouteCollection
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $routeCollection = new RouteCollection();

        $config = $container->get('Config');

        foreach ($config['router'] as $name => $route) {
            $routeCollection->add(
                $name,
                new Route(
                    $route['path'],
                    $route['defaults'] ?: [],
                    $route['requirements'] ?: [],
                    $route['options'] ?: [],
                    $route['host'] ?: '',
                    $route['schemes'] ?: [],
                    $route['methods'] ?: [],
                    $route['condition'] ?: ''
                )
            );
        }

        return $routeCollection;
    }

}
