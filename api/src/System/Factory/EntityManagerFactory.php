<?php


namespace System\Factory;


use Doctrine\Common\Cache\RedisCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Log\LoggerInterface;
use System\Debug\QueryLogger;

/**
 * Class EntityManagerFactory
 * @package System\Factory
 */
class EntityManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return EntityManager|object
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        $redis = new \Redis();
        $redis->connect(
            $config['doctrine']['redis_cache']['host'],
            $config['doctrine']['redis_cache']['port']
        );

        $cache = new RedisCache();
        $cache->setRedis($redis);

        $setup = Setup::createAnnotationMetadataConfiguration(
            [__DIR__ . '/../../App/Entity'],
            false,
            __DIR__ . '/../../../data/doctrine/Proxy',
            $cache
        );

        $em = EntityManager::create($config['doctrine']['connection'], $setup);

        if ($config['doctrine']['debug']['enabled']) {
            $em->getConnection()
                ->getConfiguration()
                ->setSQLLogger(
                    new QueryLogger(
                        $em->getConnection()->getDatabasePlatform(),
                        $config['doctrine']['debug']['query_types'],
                        $container->get(LoggerInterface::class)
                    )
                );
        }

        return $em;
    }

}
