<?php


namespace System\Factory;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPSocketConnection;

/**
 * Class AmqpChanelFactory
 * @package System\Factory
 */
class AmqpChanelFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|AMQPChannel
     * @throws \Exception
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config')['amqp'];

        if (!$config) {
            throw new ServiceNotCreatedException('Amqp configuration not provided');
        }

        return new AMQPChannel(
            new AMQPSocketConnection(
                $config['host'],
                $config['port'],
                $config['user'],
                $config['pass']
            )
        );
    }

}
