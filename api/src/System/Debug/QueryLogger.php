<?php


namespace System\Debug;

use Doctrine\DBAL\Logging\SQLLogger;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Psr\Log\LoggerInterface;

/**
 * A SQL logger that logs to the standard output and
 * subtitutes params to get a ready to execute SQL sentence
 * @author  dsamblas@gmail.com
 */
class QueryLogger implements SQLLogger
{
    const QUERY_TYPE_SELECT = "SELECT";
    const QUERY_TYPE_UPDATE = "UPDATE";
    const QUERY_TYPE_INSERT = "INSERT";
    const QUERY_TYPE_DELETE = "DELETE";
    const QUERY_TYPE_CREATE = "CREATE";
    const QUERY_TYPE_ALTER = "ALTER";

    /** @var AbstractPlatform */
    private $dbPlatform;

    /** @var array */
    private $loggedQueryTypes;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * QueryLogger constructor.
     * @param AbstractPlatform $dbPlatform
     * @param array $loggedQueryTypes
     */
    public function __construct(AbstractPlatform $dbPlatform, array $loggedQueryTypes = array(), LoggerInterface $logger)
    {
        $this->dbPlatform = $dbPlatform;
        $this->loggedQueryTypes = $loggedQueryTypes;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function startQuery($sql, array $params = null, array $types = null)

    {
        if ($this->isLoggable($sql)) {
            if (!empty($params)) {
                foreach ($params as $key => $param) {
                    $type = Type::getType($types[$key]);
                    $value = $type->convertToDatabaseValue($param, $this->dbPlatform);
                    $sql = join(var_export($value, true), explode('?', $sql, 2));
                }

            }
            $this->logger->debug($sql);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {

    }

    /**
     * @param $sql
     * @return bool
     */
    private function isLoggable($sql)
    {
        if (empty($this->loggedQueryTypes)) return true;
        foreach ($this->loggedQueryTypes as $validType) {
            if (strpos($sql, $validType) === 0) return true;
        }
        return false;
    }
}
