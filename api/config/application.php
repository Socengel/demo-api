<?php

return [
    'router' => [
        'index' => [
            'path' => '/',
            'defaults' => [
                '_controller' => \App\Controller\IndexController::class
            ]
        ],
        'create-products' => [
            'path' => '/product/create',
            'defaults' => [
                '_controller' => \App\Controller\Product\CreateProductsController::class
            ],
            'methods' => [
                'POST'
            ]
        ],
        'order-create' => [
            'path' => '/order/create',
            'defaults' => [
                '_controller' => \App\Controller\Order\CreateOrderController::class
            ],
            'methods' => [
                'POST'
            ]
        ],
        'order-pay' => [
            'path' => '/order/pay/{id}',
            'defaults' => [
                '_controller' => \App\Controller\Order\PayOrderController::class
            ],
            'requirements' => [
                'id' => '\d+'
            ],
            'methods' => [
                'POST'
            ]
        ]
    ],
    'amqp' => [
        'host' => 'rabbitmq',
        'port' => '5672',
        'user' => 'monolog',
        'pass' => '123456'
    ],
    'doctrine' => [
        'debug' => [
            'enabled' => false,
            'query_types' => [
                \System\Debug\QueryLogger::QUERY_TYPE_UPDATE,
                \System\Debug\QueryLogger::QUERY_TYPE_INSERT,
                \System\Debug\QueryLogger::QUERY_TYPE_DELETE
            ]
        ],
        'redis_cache' => [
            'host' => 'redis',
            'port' => 6379
        ],
        'connection' => [
            'driver' => 'pdo_mysql',
            'host' => 'db',
            'user' => 'demo',
            'password' => '123456',
            'dbname' => 'demo',
        ],
    ],
    'service_manager' => [
        'invokables' => [
            \App\Controller\IndexController::class
        ],
        'factories' => [
            // Контроллеры
            \App\Controller\Order\CreateOrderController::class => \App\Controller\Order\Factory\CreateOrderFactory::class,
            \App\Controller\Order\PayOrderController::class => \App\Controller\Order\Factory\PayOrderFactory::class,
            \App\Controller\Product\CreateProductsController::class => \App\Controller\Product\Factory\CreateProductsFactory::class,
            // Сервисы
            \App\Service\OrderService::class => \App\Service\Factory\OrderServiceFactory::class,
            \App\Service\ProductService::class => \App\Service\Factory\ProductServiceFactory::class,
            // Ядро
            \Psr\Log\LoggerInterface::class => \System\Factory\LogFactory::class,
            \PhpAmqpLib\Channel\AMQPChannel::class => \System\Factory\AmqpChanelFactory::class,
            \Doctrine\ORM\EntityManager::class => \System\Factory\EntityManagerFactory::class,
            \Symfony\Component\Routing\RouteCollection::class => \System\Factory\RouteCollectionFactory::class,
            \Symfony\Component\Routing\Matcher\UrlMatcher::class => \System\Factory\UrlMatcherFactory::class,
        ]
    ],
];
