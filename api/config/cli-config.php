<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

include __DIR__ . '/../vendor/autoload.php';

$app = \System\Application::init(require __DIR__ . '/application.php');

return ConsoleRunner::createHelperSet($app->getContainer()->get(\Doctrine\ORM\EntityManager::class));
