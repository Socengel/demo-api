<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200118205934 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE demo_product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE demo_order (id INT AUTO_INCREMENT NOT NULL, status INT NOT NULL, total INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE demo_order_products (order_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_D4C50F248D9F6D38 (order_id), INDEX IDX_D4C50F244584665A (product_id), PRIMARY KEY(order_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE demo_order_products ADD CONSTRAINT FK_D4C50F248D9F6D38 FOREIGN KEY (order_id) REFERENCES demo_order (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE demo_order_products ADD CONSTRAINT FK_D4C50F244584665A FOREIGN KEY (product_id) REFERENCES demo_product (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE demo_order_products DROP FOREIGN KEY FK_D4C50F244584665A');
        $this->addSql('ALTER TABLE demo_order_products DROP FOREIGN KEY FK_D4C50F248D9F6D38');
        $this->addSql('DROP TABLE demo_product');
        $this->addSql('DROP TABLE demo_order');
        $this->addSql('DROP TABLE demo_order_products');
    }
}
