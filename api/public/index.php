<?php

// подключаем composer
include __DIR__ . '/../vendor/autoload.php';

//  переводим указатель директории на корневую папку для удобного указания путей относительно корня
chdir(dirname(__DIR__));

// запускаем приложение
(\System\Application::init(require 'config/application.php'))->run();
